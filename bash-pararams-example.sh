#!/usr/bin/env bash

NO_DOWNLOAD=FALSE
NO_LOGS=FALSE

function help() {
  echo ""
  echo "Help:"
  echo ""
  echo "-nd | -nodownload | --nodownload      Disable download newest dump, use dump which is downloaded"
  echo "-nl | -no-logs | --no-logs            Disable download enitity logs (faster import)"
  echo ""
}


while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -nd | -nodownload | --nodownload)
            NO_DOWNLOAD=TRUE
            ;;
        -nl | -no-logs | --no-logs)
            NO_LOGS=TRUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            help
            exit 1
            ;;
    esac
    shift
done

echo "NODOWNLOAD $NO_DOWNLOAD"
echo "NO_LOGS $NO_LOGS"
exit
