#!/bin/bash

# usage: bash slackpost.sh <channel> <message>
WEBHOOK_URL="https://hooks.slack.com/services/XXXXX/XXXX/XXXXXX" # SLACK WEBHOOK URL


channel=$1
text=$2

if [[ $channel == "" ]]
then
        echo "No channel specified"
        exit 1
fi

if [[ $text == "" ]]
then
        echo "No text specified"
        exit 1
fi

escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" )
json="{\"channel\": \"#$channel\", \"text\": \"$escapedText\"}"

curl -s -d "payload=$json" "$WEBHOOK_URL" >> /dev/null
