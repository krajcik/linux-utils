echo Switch version to php$1

if [ -n "$1" ]; then
  echo Switch version to php$1
else
  echo "Argument php not passed. Example using: sh phpSwticher.sh 7.2"
  exit
fi


apt-get install php$1-dom -y
apt-get install php$1-gmagick -y
apt-get install php$1-SimpleXML -y
apt-get install php$1-ssh2 -y
apt-get install php$1-xml -y
apt-get install php$1-xmlreader -y
apt-get install php$1-curl -y
apt-get install php$1-date -y
apt-get install php$1-exif -y
apt-get install php$1-filter -y
apt-get install php$1-ftp -y
apt-get install php$1-gd -y
apt-get install php$1-hash -y
apt-get install php$1-iconv -y
apt-get install php$1-imagick -y
apt-get install php$1-json -y
apt-get install php$1-libxml -y
apt-get install php$1-mbstring -y
apt-get install php$1-mysqli -y
apt-get install php$1-openssl -y
apt-get install php$1-pcre -y
apt-get install php$1-posix -y
apt-get install php$1-sockets -y
apt-get install php$1-SPL -y
apt-get install php$1-tokenizer -y
apt-get install php$1-zlib -y
apt-get install php$1-gmagick -y
apt-get install php$1-ssh2 -y

echo "[DONE] ALL EXTENSIONS FOR PHP$1 INSTALLED"