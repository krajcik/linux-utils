function backupFailed {
  echo "Nepodarilo se vytvorit zalohu databaze. Zkontroluj jestli container nezmenil svuj hash a log soubor v $error" | sendemail -f krajcik-admin@email.cz -t vyvoj@autokora.cz -u "POZOR NEZDARILA SE ZALOHA DATABAZE" -s smtp.seznam.cz:25 -o tls=no -xu my-email@email.cz -xp my-email-password
  echo "[ERROR] DB backup failed"
  exit 1
}

echo "[INFO] Start backup db. Please wait ..."

d=$(date '+%d-%m-%Y_%H_%M_%S')
dump=/mnt/volume-project/dbdumps/dump_"$d".sql
error=/mnt/volume-project/dbdumps/error-"$d".txt

docker exec 31e87a5ace3f pg_dump -U project app_prod >$dump 2>$error || backupFailed

echo "[SUCESS] DB backup created"

echo "[INFO] Start upload backup to dropbox"
bash /root/dropbox_uploader.sh -s upload /mnt/volume-project/dbdumps/*.sql /project-node/db-dumps/
echo "[SUCCESS] Backup uploaded to dropbox"

echo "[INFO] Start clear old backups"
rm -rf /mnt/volume-project/dbdumps/*.sql
echo "[SUCCESS] Old backups cleared"
exit 0
