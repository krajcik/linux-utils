echo "CLEAN-DISK.SH"

echo "[INFO] Remove docker container logs"
sh -c 'truncate -s 0 /var/lib/docker/containers/*/*-json.log'

echo "[INFO] Apt clean"
apt-get clean
apt-get autoclean
apt-get autoremove

echo "[INFO] Clean systemd journal logs."
journalctl --vacuum-time=2d