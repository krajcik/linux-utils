#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

checkUrlHealth() {
    local url=${1:-http://localhost:8080}
    local code=${2:-200}
    local status=$(curl --head --location --connect-timeout 5 --write-out %{http_code} --silent --output /dev/null ${url})

  if [[ $status == ${code} ]]; then
    printf "${GREEN}[OK][$url] \n"
  else
    # try it again
    sleep 20
    local url=${1:-http://localhost:8080}
    local code=${2:-200}
    local status=$(curl --head --location --connect-timeout 5 --write-out %{http_code} --silent --output /dev/null ${url})

    if [[ $status == ${code} ]]; then
      printf "${GREEN}[OK][$url] \n"
    else
      printf "${RED}[ERROR][$url] is not available \n"
      bash slackpost.sh dostupnost "[ERROR] $url is not available, return status $status"
    fi
  fi
}

checkHealth() {
    checkUrlHealth https://myweb.com
}



## MAIN ##
checkHealth

exit 0
